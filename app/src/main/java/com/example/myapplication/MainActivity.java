package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Button btnhal1, btnhal2;
    private TextView displaytext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnhal1 = findViewById(R.id.btnhal1);
        btnhal2 = findViewById(R.id.btnhal2);

        btnhal1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick( View v) {
                Intent intentDua = new Intent(MainActivity.this, secondactivity.class);
                startActivity(intentDua);
            }
        });

        btnhal2.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Intent intentTiga = new Intent(MainActivity.this, thirdactivity.class);
                startActivity(intentTiga);
            }
        });

    }
}