package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class FirstFragment extends Fragment {
    private Button btnGanti;
    private EditText tulisan;
    @Nullable
    @Override
    public View onCreateView (@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first, container, false);
        btnGanti = view.findViewById(R.id.btngantitext);
        tulisan = view.findViewById(R.id.tulisan);
        btnGanti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent intentmain = new Intent(getActivity(), MainActivity.class);
                String isian = tulisan.getText().toString();
                intentmain.putExtra("PesanDariFragment", isian);
                startActivity(intentmain);
            }
        });
        return view;
    }

}
